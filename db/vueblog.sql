-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2020 at 08:42 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vueblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Maribel Huels', '2020-01-16 15:20:07', '2020-01-16 15:20:07'),
(2, 'Jacey Goldner', '2020-01-16 15:20:07', '2020-01-16 15:20:07'),
(3, 'Prof. Colten Purdy Jr.', '2020-01-16 15:20:07', '2020-01-16 15:20:07'),
(4, 'Mango', '2020-01-17 15:15:47', '2020-01-17 15:15:47'),
(5, 'Apple', '2020-01-17 16:27:20', '2020-01-17 16:27:20'),
(6, 'Mango', '2020-01-18 12:54:58', '2020-01-18 12:54:58'),
(7, 'Orange', '2020-01-18 13:00:15', '2020-01-18 13:00:15'),
(8, 'New One', '2020-01-18 13:14:40', '2020-01-18 13:14:40'),
(9, 'New Two', '2020-01-18 13:17:44', '2020-01-18 13:17:44'),
(10, 'Three', '2020-01-18 13:33:16', '2020-01-18 13:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_16_195606_create_categories_table', 1),
(5, '2020_01_16_202354_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `cat_id`, `user_id`, `title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Non id doloribus nulla nostrum repudiandae rem.', 'Et aut expedita beatae doloremque saepe ut labore omnis.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(2, 2, 2, 'Blanditiis laboriosam labore laboriosam harum eos.', 'Corporis deserunt ut iure sapiente dicta quibusdam consequuntur.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(3, 1, 2, 'Eius mollitia velit totam quasi totam enim ipsum quia.', 'Autem non delectus ipsam.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(4, 3, 1, 'Sed omnis itaque vel repellendus.', 'Aut quo est veritatis dicta.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(5, 3, 1, 'Nam nihil facere blanditiis et iste laboriosam iste.', 'Velit sed et harum porro maiores.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(6, 1, 1, 'Cum eaque dignissimos possimus eos cupiditate.', 'Dolor aut ipsa suscipit sit est nesciunt vitae.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(7, 1, 1, 'Explicabo veritatis iste iure optio.', 'Necessitatibus et iste error corporis quaerat eum est.', '1579210431.jpg', '2020-01-16 15:33:51', '2020-01-16 15:33:51'),
(8, 2, 3, 'Est aspernatur accusantium aut nemo voluptatem sapiente labore architecto.', 'Mollitia sint quisquam corporis placeat magni.', '1579210431.jpg', '2020-01-16 15:33:52', '2020-01-16 15:33:52'),
(9, 2, 1, 'Aut libero esse sapiente voluptate voluptate occaecati vero expedita.', 'Et ipsam repellat qui architecto.', '1579210431.jpg', '2020-01-16 15:33:52', '2020-01-16 15:33:52'),
(10, 1, 2, 'Qui saepe ut molestiae architecto fuga tempore quod id.', 'In numquam ea aut perspiciatis temporibus possimus consequuntur beatae.', '1579210431.jpg', '2020-01-16 15:33:52', '2020-01-16 15:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Vern Turner', 'kayden21@example.org', '2020-01-16 15:20:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wocacJv3tg', '2020-01-16 15:20:07', '2020-01-16 15:20:07'),
(2, 'Devin Abbott', 'rau.haylie@example.com', '2020-01-16 15:20:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XP3Rvm3fTU', '2020-01-16 15:20:07', '2020-01-16 15:20:07'),
(3, 'Dr. Joanne Langworth', 'mertz.josefa@example.org', '2020-01-16 15:20:07', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '30Iq2HNuGu', '2020-01-16 15:20:07', '2020-01-16 15:20:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
