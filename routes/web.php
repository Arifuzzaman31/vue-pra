<?php

Route::get('get-category-all','CategoryController@view');

Route::get('remove-category/{id}','CategoryController@destroy');

Route::post('/store-category', 'CategoryController@store');

Route::get('get-post-all','PostController@view');

Route::get('delete-post/{id}','PostController@destroy');


Route::get('all-posts','PostController@allPost');
Route::get('/', 'SpaController@index');
// Route::get('/{any}', 'SpaController@index')->where('any', '.*');

Route::post('update/category','CategoryController@update');
