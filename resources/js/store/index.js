export default {
	base_url: "{{ url('/') }}"+'/',
	state:{
		category : [],
		post : [],
	},
	getters:{
		getCategory(state){
			return state.category
		},
		getPost(state){
			return state.post
		},
	},
	actions:{
		allPost(context){
            axios.get(base_url+'get-post-all')
              .then((response) => {
                  console.log(response.data)
                  context.commit('allposts',response.data)
              })
              .catch(error => console.log(error))
        },

	},
	mutations:{
		allposts(state,payload){
            return state.post = payload
        },
	},
}