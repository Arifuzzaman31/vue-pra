require('./bootstrap');

import Vue from 'vue'
// window.Vue = require('vue')
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import {routes} from './Route'
// const routes = require('./routes');
Vue.use(VueRouter)

Vue.use(Vuex)

import {filter} from './filter'

import storeData from "./store/index"
const store = new Vuex.Store(
    storeData,
);
import App from './views/App'
import Navbar from './views/sidenav'

// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
window.Toast = Toast;

import { Form, HasError, AlertError } from 'vform'

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
window.Form = Form;

const router = new VueRouter({
    mode: 'history',
   	routes,
});

const app = new Vue({
    el: '#app',
    components: { 
        'App' : App,
        'side-nav' : Navbar
    },
    router,
    store,
});
