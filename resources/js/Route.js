import Posts from './views/post/Post'

import ListCat from './views/category/List'
import AddCat from './views/category/AddCat'
import EditCat from './views/category/editCat'

import AddPost from './views/post/AddPost'

export const routes = [
        
        {
            path: '/posts',
            name: 'posts',
            component: Posts,
        },
        {
            path: '/add-post',
            name: 'add-post',
            component: AddPost,
        },
        {
            path: '/category-list',
            name: 'category-list',
            component: ListCat,
        },
        {
            path: '/add-category',
            // name: 'category-list',
            component: AddCat,
        },
        
        {
            path: '/category/:data/edit', // the URL accepts an accountId parameter
            component: EditCat,
            name: 'editCategory'
        }
    ]