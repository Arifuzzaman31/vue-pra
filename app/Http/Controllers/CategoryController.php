<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'cat_name' => 'required|min:3|max:30'
    	]);

    	$cat = new Category();
    	$cat->cat_name = $request->cat_name;
    	$cat->save();
    	return ['message' => 'Category Added successfull'];
    }

    public function view()
    {
    	$category = Category::paginate(5);
    	// return $category;
    	return request()->json(200,$category);
    }

    public function destroy($id)
    {
        $result = Category::find($id);
        if(count($result->posts) > 0){
            $message = [
                'type' => 'Warning',
                'message' => 'It has Children',
                'status' => 'error'
            ];
        } else {
            
        $result->delete();
        $message = [
                'type' => 'Deleted!',
                'message' => 'Category deleted successfully',
                'status' => 'success'
            ];
        }
        return response()->json($message);
    }

    public function update(Request $request)
    {
        $result = Category::find($request->id);
        $result->cat_name = $request->cat_name;
        $result->update();
        return response()->json(['message' => 'Category Updated Successfull']);
    }
}




