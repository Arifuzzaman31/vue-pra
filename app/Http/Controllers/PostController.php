<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
	public function allPost()
    {
    	// $posts = Post::with(['user','category'])->get();
    	$posts = Post::all();
    	 return $posts;
    	 return response()->json($posts,200);
    }
    public function view()
    {
    	// $posts = Post::all();
    	$posts = Post::with('user')->get();
    	return $posts;
    	// return request()->json($posts);
    }

    public function destroy($id)
    {
    	$result = Post::find($id);
        // if(count($result->posts) > 0){
        //     $message = [
        //         'type' => 'Warning',
        //         'message' => 'It has Children',
        //         'status' => 'error'
        //     ];
        // } else {
            
        $result->delete();
        $message = [
                // 'type' => 'Deleted!',
                'message' => 'Category deleted successfully',
                'status' => 'success'
            ];
        // }
        return response()->json($message);
    }
}
